from lxml import etree
import pdb


#######################
def updateLuxParam(dir_input, inputputConfig):
    root = etree.parse(dir_input+'phase.xml')            
    root = etree.parse(dir_input+'phase.xml').getroot()    

    phaseElement = root[0]
    for element in phaseElement:
        if element.tag == 'EngineParameter':
            EngineParameter = element
            break

    for element in EngineParameter:
        if element.tag == 'LuxCoreRenderEngineParameters':
            element.attrib['pixelSize']                = '{:.2f}'.format(inputputConfig.params_DART['dxy'])
            element.attrib['targetRayDensityPerPixel'] = '{:d}'.format(int(inputputConfig.params_DART['lux_rayDensity']))
            element.attrib['maximumRenderingTime']     = '{:d}'.format(int(inputputConfig.params_DART['lux_max_time']))
    
    et = etree.ElementTree(root)
    et.write(dir_input + '/phase.xml', pretty_print=True)
    print ('   update coeff_diff.xml with fluid_Gas.db in DART_maket')


