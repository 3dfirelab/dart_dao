from past.utils import old_div
import sys
import os
import random as rnd
import numpy as np
import struct
import shutil 
import pdb 
import math 
import pickle
import matplotlib as mpl
import matplotlib.pyplot as plt
from skimage import io
import importlib

DART_HOME = ""  # set here the default path of your DART folder
DART_HOME = os.path.expanduser(DART_HOME)
if len(DART_HOME) == 0:
    try:
        DART_HOME = os.environ['DART_HOME']
    except KeyError:
        DART_HOME = None

if DART_HOME is None:
    raise EnvironmentError("You need to set your DART_HOME path, either by setting the variable above or through system environment.")

DART_LOCAL = ""  # set here the default path of your DART folder
DART_LOCAL = os.path.expanduser(DART_LOCAL)
if len(DART_LOCAL) == 0:
    try:
        DART_LOCAL = os.environ['DART_LOCAL']
    except KeyError:
        DART_LOCAL = None
if DART_LOCAL is None:
    raise EnvironmentError("You need to set your DART_LOCAL path, either by setting the variable above or through system environment.")

# add the DAO python folder to current system path
sys.path.append(os.path.join(DART_HOME, "bin", "python_script", "DAO"))
# import DAO toolkit
import dao
from dao.tools.Mesh import Mesh

# Physics constants
MCO2 = 12 + 2*16    # g/mol
MCO = 12 + 16       # g/mol
MH2O = 2*1 + 16     # g/mol
m_1_soot = 1.e-21    # kg
NAVOGADRO = 6.022e23    # mol-1
MAcetone = 58.08    # g/mol

#lambertian
PHASE_GND_ID = 0
PHASE_SHEET_ID = 4
#Fluid
PHASE_ACETONE_ID = 3
TEMP_FUN_GND_ID = 0

marge = 0.01

minMassDry  = 1.e-6 #kg
minMassChar = 1.e-8 #kg
minMassAsh  = 1.e-8 #kg


#####################################################
def ensure_dir(f):
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(d)
        
        
#############################################################
def create_raster_dem(outputDir_simu,nx,ny,nz,dx,dy,dz):

    z0_dart = (1.-marge)*dz  
    
    #zero_level = 1.0663275133993011e-05
    dem = np.zeros([nx,ny]) + z0_dart
    dem[0,0] = 0

    file_dem = outputDir_simu + '/dem_raster.tif'
    io.imsave( file_dem, dem, plugin='tifffile',check_contrast=False)
    
    #file_dem = outputDir_simu + '/dem_raster.img'
    #f = open(file_dem,'wb')
    #for i in range(nx):
    #    for j in range(ny):
    #        f.write(struct.pack('d',dem[i,j]))
    #f.close()
    
    return z0_dart, dem


#############################################################
def get_lut_gas(lut, specie, ):
    idx_specie = []
    lut_temp = []
    for ii, name in enumerate(lut.name):
        if specie == name.split('_')[0].lower():
            idx_specie.append(ii)
            lut_temp.append( float(lut.name[ii].split('_')[1].split('k')[0]) )
   
    lut = lut[(idx_specie,)]
    lut_temp = np.array(lut_temp)
   
    lut_out = np.array([('m',0,'m','m',0.)]*lut.shape[0],dtype=np.dtype([ (x,y[0]) for x,y in lut.dtype.fields.items()] + [('temp',np.dtype(float))]))
    lut_out = lut_out.view(np.recarray)
    lut_out.type = lut.type
    lut_out.id = lut.id
    lut_out.ident = lut.ident
    lut_out.name = lut.name
    lut_out.temp = lut_temp

    return lut_out

#############################################################
def get_lut_veg(lut):
    idx_veg = []
    for ii, type_ in enumerate(lut.type):
        if type_ ==  'turbid':
            idx_veg.append(ii)
   
    lut = lut[(idx_veg)]
    
    return lut.view(np.recarray)

#############################################################
def getNameAndIdFromGasAndTemp(lut, specie, temperature = None):

    if specie == 'soot': 
        idx = np.where(lut.name == 'soot_poitou')
        return lut.ident[idx][0], lut.id[idx][0]
    
    elif specie == 'vegetation':  # for fluid veg
        idx = np.where(lut.name == 'rayleigh_1_0')
        #idx = np.where(lut.name == 'hg_1_1_1_1_1')
        #pdb.set_trace()
        return lut.ident[idx][0], lut.id[idx][0]
    
    elif specie == 'dryVeg':  
        idx = np.where(lut.name == 'grass_rye')
        return lut.ident[idx][0], lut.id[idx][0]
    
    elif specie == 'charVeg':  
        idx = np.where(lut.name == 'reflect_equal_0_trans_equal_0_0')
        return lut.ident[idx][0], lut.id[idx][0]
    
    elif specie == 'ashVeg':  
        idx = np.where(lut.name == 'grass_rye')
        return lut.ident[idx][0], lut.id[idx][0]

    else: #deal with gas
        idx_ = np.abs( lut.temp - temperature).argmin()

        return lut.ident[idx_], lut.id[idx_]


#############################################################
def addPlotDescription2Mockup(inputConfig, flag,\
                              i, j, k, i0, j0, k0, dxy, dz, dxyD, dzD,  
                              scene, lut, 
                              soot_fv_max, soot_fv_threshold, 
                              aerosol_tracer_max, aerosol_tracer_threshold ) :
  
    # some aliases on dao types
    TriangleProperty = dao.TriangleProperty
    Triangle = dao.Triangle
    Turbid = dao.Turbid
    Fluid = dao.Fluid
    Matrix4 = dao.Matrix4

    TEMP_FUN_ID = 0
    lut_co2, lut_co, lut_h2o, lut_veg, lut_opticProp_coeff_diff = lut
    nplot_gas,nplot_plu = 0, 0
    nplot_veg,nplot_char,nplot_ash = 0, 0, 0

    '''
    Each cell is a plot (a truncated cylinder with a base and a height)
    Each plot needs :
    - the coordonates of each corner (4) of the base
    - the height of the base
    - the height of the plot
    - the number of different particles and for each one :
        - the number density
        - the index and name of the optical property file (the same as in
        "Optical & temperature properties")
    - the index and name of the temperature function (the same as in
    "Optical & temperature properties") which is ignored since the
    temperature.txt file will be provided to DART
    '''
    
    # Base ABCD of the plot (A,B,C,D in the anticlockwise)
    #xA = i*dxy      + dxy*0.001
    #yA = j*dxy      + dxy*0.001
    #xB = (i+1)*dxy  - dxy*0.001
    #yB = j*dxy      + dxy*0.001
    #xC = (i+1)*dxy  - dxy*0.001
    #yC = (j+1)*dxy  - dxy*0.001
    #xD = i*dxy      + dxy*0.001
    #yD = (j+1)*dxy  - dxy*0.001
    
    # height_base = (k+1)*dz  # flame "floating" above the ground
    height_base = (k-k0)*dz + dzD*1.01 # Ronan: add k+1, this is for the DEM
    height_plot = dz - dzD*0.02
    max_height_plot = -999 
    
    #nb_part = 4
    if flag == 'fire': 
        m_CO2  = scene.CO2[i,j,k]   * 1.e3 #g
        m_CO   = scene.CO[i,j,k]    * 1.e3
        m_H2O = scene.H2O[i,j,k]    * 1.e3 
        sootXMLvalue = scene.fv[i,j,k]
        aerosolXMLvalue = 0 
    elif flag == 'atm':
        m_CO2  = 0.
        m_CO   = 0.
        m_H2O = scene.rvap[i,j,k]    * 1.e3 
        sootXMLvalue = None 
        aerosolXMLvalue = 0 



    #m_soot = scene.soot[i,j,k] #* 1.e3
    
    dCO2 = round(old_div(m_CO2, MCO2) * NAVOGADRO )#* 1.e-15)     # because densities are in 1e15 m-3 in DART inputs
    dCO  = round(old_div(m_CO, MCO)  * NAVOGADRO )#* 1.e-15)
    dH2O = round(old_div(m_H2O, MH2O) * NAVOGADRO )# * 1.e-15)
    #dsoot = round(m_soot / m_1_soot )#* 1.e-15)          ############################## check optical properties of soot
  
    fluid = []
    loc   = []


    #
    # add flame materials
    #
    if flag == 'fire': 
        #try: 
        if (scene.fv[i,j,k] >= soot_fv_max * soot_fv_threshold): 
        ############################################# just to reduce the number of plots
            max_height_plot = height_base+height_plot
        
            flag_skip_soot = False
            if ('onlyGas' in inputConfig.params_DART.keys()):
                if inputConfig.params_DART['onlyGas']:     
                    flag_skip_soot = True
            if not(flag_skip_soot):
                NAME_PHASE_FUN_SOOT, PHASE_FUN_ID_SOOT = getNameAndIdFromGasAndTemp(lut_opticProp_coeff_diff, 'soot' )  
                fluid.append(Fluid(density=sootXMLvalue, fluidOpticalPropertyID=PHASE_FUN_ID_SOOT, temperatureID=TEMP_FUN_ID))
                loc.append(( (dxy*(i-i0)+dxyD*marge), (dxy*(j-j0)+dxyD*marge), (dz*(k-k0)+dzD*(1.+marge)) ))

            if dCO2 > 0: 
                NAME_PHASE_FUN_CO2 ,PHASE_FUN_ID_CO2 = getNameAndIdFromGasAndTemp(lut_co2, 'co2', temperature=scene.Temp[i,j,k])
                fluid.append(Fluid(density=dCO2, fluidOpticalPropertyID=PHASE_FUN_ID_CO2, temperatureID=TEMP_FUN_ID))
                loc.append(( (dxy*(i-i0)+dxyD*marge), (dxy*(j-j0)+dxyD*marge), (dz*(k-k0)+dzD*(1.+marge)) ))
            
            if dCO > 0: 
                NAME_PHASE_FUN_CO ,PHASE_FUN_ID_CO = getNameAndIdFromGasAndTemp(lut_co, 'co', temperature=scene.Temp[i,j,k])
                fluid.append(Fluid(density=dCO, fluidOpticalPropertyID=PHASE_FUN_ID_CO, temperatureID=TEMP_FUN_ID))
                loc.append(( (dxy*(i-i0)+dxyD*marge), (dxy*(j-j0)+dxyD*marge), (dz*(k-k0)+dzD*(1.+marge)) ))

            if dH2O > 0: 
                NAME_PHASE_FUN_H2O ,PHASE_FUN_ID_H2O = getNameAndIdFromGasAndTemp(lut_h2o, 'h2o', temperature=scene.Temp[i,j,k])
                fluid.append(Fluid(density=dH2O, fluidOpticalPropertyID=PHASE_FUN_ID_H2O, temperatureID=TEMP_FUN_ID))
                loc.append(( (dxy*(i-i0)+dxyD*marge), (dxy*(j-j0)+dxyD*marge), (dz*(k-k0)+dzD*(1.+marge)) ))
            
            nplot_gas = 1
        #except AttributeError as e:
        #    #print "AttributeError raised: {1}".format(e.strerror)
        #    pass
    

    #
    # Vegetation plot
    #
    key_temp_veg = 'Temp_veg'
    if 'lux_vegModel' in inputConfig.params_DART.keys():
        if inputConfig.params_DART['lux_vegModel'] == 'turbid': key_temp_veg = 'Temp_veg2'
        lux_vegModel = inputConfig.params_DART['lux_vegModel']
    else: 
        lux_vegModel = 'fluid' # if not defined, for example in FT mode, then we use default fluid
    

    if flag == 'fire':
        if lux_vegModel == 'fluid':
            try:
                #flag_ = (scene.fv[i,j,k] < soot_fv_max * soot_fv_threshold) #if inputConfig.params_DART['useLux'] else True 
                #if (scene.kappa_veg[i,j,k] > 0) & (flag_): 
                #if (scene.Temp_veg[i,j,k] > T_ambient) & (k==1):
                #if (scene.MassDry[i,j,k] > minMassDry): 
                #if (scene[key_temp_veg][i,j,k] > 0) :
                if (scene.Temp_veg[i,j,k] > 0):
                    max_height_plot = height_base+height_plot     
                    #print(scene[key_temp_veg][i,j,k], scene.kappa_veg[i,j,k])
                    #print(scene.kappa_veg[i-1:i+2,j-1:j+2,k]) 
                    #print(i,j,k) 
                    #print(scene.kappa_veg[i-1:i+2,j-1:j+2,1]) 
                    # Vegetation air plot
                    kveg = scene.kappa_veg[i,j,k]
                    NAME_PHASE_FUN_VEG, PHASE_FUN_ID_VEG   = getNameAndIdFromGasAndTemp(lut_opticProp_coeff_diff, 'vegetation' ) 
                    fluid.append(Fluid(density=kveg, fluidOpticalPropertyID=PHASE_FUN_ID_VEG, temperatureID=TEMP_FUN_ID))
                    loc.append(( (dxy*(i-i0)+dxyD*marge), (dxy*(j-j0)+dxyD*marge), (dz*(k-k0)+dzD*(1.+marge)) ))
                   
                    #pdb.set_trace()
                    nplot_veg += 1
            
            except AttributeError as e:
                #print "AttributeError raised: {1}".format(e.strerror)
                pass 
        
        elif lux_vegModel == 'turbid':
          
            # Dry Veg turbid plot
            if (scene.MassDry[i,j,k] > minMassDry): 
                max_height_plot = height_base+height_plot     
                NAME_PHASE_FUN_VEG, PHASE_FUN_ID_VEG   = getNameAndIdFromGasAndTemp(lut_veg, 'dryVeg' ) 
                # MORVAN formulation of LAI. DOI: 10.1007/s10694-010-0160-2
                LAI =0.5 *  (scene.MassDry[i,j,k]/(dxy**2*dz) / scene.RhoBDry[i,j,k]) * scene.Surf2Vol[i,j,k] * dz  
                fluid.append(Turbid(lai= LAI, vegetationOpticalPropertyID=PHASE_FUN_ID_VEG, temperatureID=TEMP_FUN_ID))
                loc.append(( (dxy*(i-i0)+dxyD*marge), (dxy*(j-j0)+dxyD*marge), (dz*(k-k0)+dzD*(1.+marge)) ))
                nplot_veg += 1
                
            # Char turbid plot
            if (scene.MassChar[i,j,k] > minMassChar): 
                max_height_plot = height_base+height_plot     
                NAME_PHASE_FUN_VEG, PHASE_FUN_ID_VEG   = getNameAndIdFromGasAndTemp(lut_veg, 'charVeg' ) 
                if (scene.RhoBChar[i,j,k]== 0) | (scene.Surf2Vol[i,j,k] ==0)  : pdb.set_trace() 
                LAI =0.5 *  (scene.MassChar[i,j,k]/(dxy**2*dz) / scene.RhoBChar[i,j,k]) * scene.Surf2Vol[i,j,k] * dz
                fluid.append(Turbid(lai= LAI, vegetationOpticalPropertyID=PHASE_FUN_ID_VEG, temperatureID=TEMP_FUN_ID))
                loc.append(( (dxy*(i-i0)+dxyD*marge), (dxy*(j-j0)+dxyD*marge), (dz*(k-k0)+dzD*(1.+marge)) ))
                nplot_char += 1

            # Ash turbid plot
            if False: #(scene.MassAsh[i,j,k] > minMassAsh): 
                max_height_plot = height_base+height_plot     
                NAME_PHASE_FUN_VEG, PHASE_FUN_ID_VEG   = getNameAndIdFromGasAndTemp(lut_veg, 'ashVeg' ) 
                #lai = 
                fluid.append(Turbid(lai= 3, vegetationOpticalPropertyID=PHASE_FUN_ID_VEG, temperatureID=TEMP_FUN_ID))
                loc.append(( (dxy*(i-i0)+dxyD*marge), (dxy*(j-j0)+dxyD*marge), (dz*(k-k0)+dzD*(1.+marge)) ))
                nplot_ash += 1

        else: 
            print ('##################')
            print ('##################')
            print ("you need to define inputConfig.params_DART['lux_vegModel'] ")
            sys.exit()
 
    return fluid, loc, nplot_veg, nplot_char, nplot_ash, nplot_gas, nplot_plu, max_height_plot



#############################################################
def addFireScenePlot2Mockup(inputConfig, mockup, Firescene, lut, fv_thresholds, dir_current_simulation):
    

    nx,ny,nz = mockup.getMockupDimension()
    dxy,dz = mockup.getCellSize()

    Lx, Ly, Lz = nx*dxy, ny*dxy, nz*dz

    line_t = ''
    nplot_veg, nplot_char, nplot_ash, nplot_gas,nplot_plu = 0, 0, 0, 0, 0
    
    soot_fv_max = Firescene.fv[:,:,:].max()
    #aerosol_fv_max = Firescene.fv_aerosol[:,:,:].max()
    #fv_threshold = 0.01
    soot_fv_threshold = fv_thresholds[0]

    index = np.where( Firescene.fv > soot_fv_max * soot_fv_threshold)
    print('   max       soot fv = {:}'.format(soot_fv_max)) 
    print('   threshold soot fv = {:}'.format(soot_fv_max * soot_fv_threshold)) 
    print('   nbre plot in the flame =', len(index[0]))
    
    '''
    if aerosol_tracer_threshold is not None: 
        index = np.where( Firescene.fv_aerosol > aerosol_fv_max * aerosol_fv_threshold)
        print('   max       aerosol fv = {:2.5e}'.format(aerosol_fv_max)) 
        print('   threshold aerosol fv = {:2.5e}'.format(aerosol_fv_max * aerosol_fv_threshold)) 
        print('   nbre plot in the plume =', len(index[0]))
    else: 
        if Firescene.fv_aerosol.max()!=0:  
            print ('stop here, there is aerosol concentration but no aerosol_fv_threshold defined in config file')
            sys.exit()
    '''

    #dump ground temperature in the file temperature.txt (first layer)
    line_t = []
    for i in range(nx):
        for j in range(ny):
            dT = Firescene.Temp_grd[i,j,0]
            #if dT<0.1:    # DART does not accept null temperatures for non empty cells
            #    dT = 0.1
            #ft.write("{0:8.3f} ".format(dT))
            line_t.append("{0:8.3f} ".format(dT))
            #ft.write('\n')
            #line_t.append('\n')
        #ft.write('\n')
        line_t.append('\n')
    line_t.append('\n')
   
    max_height_plot = -999

    #main loop 
    ii = 0
    fluids = []
    locs   = []
    key_temp_veg = 'Temp_veg'
    if 'lux_vegModel' in inputConfig.params_DART.keys():
        if inputConfig.params_DART['lux_vegModel'] == 'turbid': key_temp_veg = 'Temp_veg2'
    #flag_mm = True
    for k in range(nz-1):
        for i in range(nx):
            for j in range(ny):

                nplot_veg_, nplot_char_, nplot_ash_, nplot_gas_, nplot_plu_ = 0, 0, 0, 0, 0

                # Write the temperature of the voxel (here voxel == cell) in temperature.txt
                #-------
                if (Firescene.MassDry[i,j,k] + Firescene.MassChar[i,j,k]) > min([minMassDry,minMassChar]): 
                    dT = Firescene[key_temp_veg][i,j,k]
                else:
                    dT = Firescene.Temp[i,j,k]
               
                if dT < 0 : pdb.set_trace()
                #if dT<0.1:    # DART does not accept null temperatures for non empty cells
                #    dT = 0.1
               
                #ft.write("{0:8.3f} ".format(dT))
                line_t.append("{0:8.3f} ".format(dT))
                # end of writing in temperature.txt
                
                i00 = 0 
                j00 = 0
                k00 = 0
                
                #if flag_mm:
                #add plot
                #-------
                if True: #nplot_veg < 5000: 
                    if soot_fv_max == 0: soot_fv_max = 1.e6
                    fluids_, locs_, nplot_veg_, nplot_char_, nplot_ash_, nplot_gas_, nplot_plu_, max_height_plot_ = \
                             addPlotDescription2Mockup (inputConfig, 'fire',\
                                                        i,j,k, i00, j00, k00, dxy, dz, dxy, dz,
                                                        Firescene, lut, 
                                                        soot_fv_max, soot_fv_threshold, 
                                                        None, None )

                    [fluids.append(fluid_) for fluid_ in fluids_]
                    [locs.append(loc_)     for loc_   in locs_]
                
                
                #    if len(fluids_)>0: 
                #        flag_mm = False

                nplot_veg+=nplot_veg_ ; nplot_char+=nplot_char_ ; nplot_ash+=nplot_ash_ ; nplot_gas+=nplot_gas_ ; nplot_plu+=nplot_plu_
                max_height_plot = max([max_height_plot_,max_height_plot])
                #print ('{:.2f} %\r'.format(100.*ii/(nx*ny*nz)),end='')
                
                #print (i,j,k)
                #if nplot_veg == 1: break 
                
                sys.stdout.flush()
                ii+=1
                #end j
            
            #ft.write('\n')
            line_t.append('\n')
            #end i
            
            #if nplot_veg == 1: break  

        #ft.write('\n')
        line_t.append('\n')
        #end k
        #if nplot_veg == 1: break 
    
    print('   There are {0} plots with vegetation.'.format(nplot_veg))
    print('   There are {0} plots with char.'.format(nplot_char))
    print('   There are {0} plots with ash.'.format(nplot_ash))
    print('   There are {0} plots with gas or soot.'.format(nplot_gas))
    print('   There are {0} plots in the plume.'.format(nplot_plu))
    print('   And max temperature on the ground is {:.2f}'.format(Firescene.Temp_grd[:,:,0].max()))
    #add fluid to mockup
    for fluid_,loc_ in zip(fluids,locs):
        #mockup.addPatch(fluid_, loc_, [dxy*(1-2*marge),dxy*(1-2*marge),dz*(1-2*marge)])
        if inputConfig.params_DART['useLux']:
            mockup.addPatch(fluid_, loc_, [(1-2*marge)*dxy ,(1-2*marge)*dxy,(1-2*marge)*dz])
        
        else:
            #loc__ = (np.array(loc_)/dxy).tolist()
            try:
                loc__ = (np.array(loc_[:2])/dxy).tolist() + (np.array([loc_[2]])/dz).tolist() 
            except: 
                pdb.set_trace()
            if loc__[0] >= nx : pdb.set_trace()
            if loc__[1] >= ny : pdb.set_trace()
            if loc__[2] >= nz : pdb.set_trace()
            mockup.addPatch([fluid_], loc__, [1,1,1])

    # write temperature.txt
    print('   write: ' + dir_current_simulation+'input/temperatures.txt')
    with open(dir_current_simulation+'input/temperatures.txt','w') as ft:
        ft.writelines(line_t)
    
    return mockup


