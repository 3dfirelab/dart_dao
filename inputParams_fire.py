params_DART = {
              #run parameters
              'dirData': './Test/', 
              'name_simulation': 'fdsExample_multiproc',
              'time':39,
              'dxy':0.04,
              'dz':0.04,
              'fv_threshold': [0.1],   # keep point where fv >= fv_max * fv_threshold 

              #dart config
              'useLux': False,   # if False use FT             
              'lux_max_time': 24, #  
              'lux_rayDensity': 100, # 
              'lux_vegModel': 'turbid', # 'turbid' or 'fluid' 
                                        # if fluid it uses grass_rye which is already set in xml template and DAO
                                        # if fluid it uses rayleigh_1_0 with adjusted cross section following classical method in fire model 
              #
              'flag_run_sensitivity': False,  #need to stay at Flase
}


