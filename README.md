# example of DAO implementation to load a cartesian gridded scene in DART #


### What is this repository for? ###

* the repository uses a very simple simulate scene generated with the Fire Dynamics Simulator ([FDS](https://pages.nist.gov/fds-smv/))
* it uses the DAO tools of DART to load the scene into the DART input file using a set of parameter defined in`inputParams_fire.py` 

### How do I get set up? ###

* it runs on jupyter notebook
* the file `jupy.yml` list the required python package to run the notebook
* you can create the same conda environment using 
```
conda env create -f environment.yml
```
for more details see doc [here](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html)
* once DART is installed, you need to define two environment variables pointing to DART home and local directories. For example in you `.bashrc` file add 
```
export DART_HOME="/home/paugam/DART587_1234/"
export DART_LOCAL="/home/paugam/DART_user_data/"
```


